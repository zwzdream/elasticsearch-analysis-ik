1.停用词[es_extra_stopword]和主词[es_extra_main]放一张表，建表语句见/sql/t_es_dynamic_dict.sql
2.修改/config/下的jdbc.properties [jdbc.update.interval 单位为秒]
  # 查询结果的时间是比某个时间A（初始默认是2020-01-01 00:00:00.0，执行一次后备，记录最后一条记录的update_time,保存在内存中）大，（即时间靠后），
  # 这样的话，之前查过的结果再次查询，就不会出来，减少资源浪费,但是这样的话，es启动后，后面需要入表的热词记录的update_time必须保证比时间A大
  # 否则es热加载词典无效，除非重启es。
  
  jdbc.update.main.dic.sql=SELECT * FROM `t_es_dynamic_dict` WHERE is_stopword = 0 and  update_time > ? order by update_time asc
  jdbc.update.stopword.sql=SELECT * FROM `t_es_dynamic_dict` WHERE is_stopword = 1 and update_time > ? order by update_time asc
3.这里的update_time要比es运行的时间(上面配置后重启es的时间)往后。另外，加入词典前会先验证词典里面有没有，没有才加