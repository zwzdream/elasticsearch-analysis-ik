    create table t_es_dynamic_dict (
      id int(11) primary key not null auto_increment,
      word varchar(50) not null default '' comment '词条',
      is_stopword tinyint(1) not null default '0' comment '是否为停止词, 1为是',
      is_deleted tinyint(1) not null default '0' comment '删除状态, 1为删除',
     -- update_time int(11) not null default '0' comment '最后更新时间',
      update_time timestamp not null DEFAULT CURRENT_TIMESTAMP  comment '最后更新时间',
      key is_stopword_idx(is_stopword),
      key is_deleted_idx(is_deleted),
      key update_time_idx(update_time)
    )  comment='ES热更新词库表';